# Métas +

Améliorez l’indexation de vos articles (et de vos autres objets) dans les moteurs et leur affichage sur les réseaux sociaux grâce aux métadonnées Dublin Core, Open Graph et Twitter Card.

## Documentation
https://contrib.spip.net/4969
