# Changelog

## Unreleased

### Added

- #31 permettre de choisir si l'on souhaite recadrer les logos en les "réduisant" ou en les "agrandissant"

## 2.4.7 - 2024-07-27

### Fixed

- Compatibilité SPIP 4.3

## 2.4.6 - 2024-06-05

### Fixed

- #29 utiliser la bonne syntaxe pour afficher une vignette cliquable pointant vers le document
- #30 éviter que le formulaire de configuration ne casse avec BigUp dans sa version 3.2.11
- #24 Notice php en moins, à piori levée sur la page d'accueil
- #27 Passer la date de publication
- #22 Éviter une erreur sur les pages minipres

## 2.4.5 - 2023-06-15

### Added

- Un changelog
